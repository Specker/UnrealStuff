// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "MyActorComponent.generated.h"




UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TIMESTEPTEST_API UMyActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	FCalculateCustomPhysics OnCalculateCustomPhysics;

	UMyActorComponent (const FObjectInitializer& ObjectInitializer) : UActorComponent(ObjectInitializer)
	{
		OnCalculateCustomPhysics.BindUObject(this, &UMyActorComponent::CustomPhysics);
	}

	void CustomPhysics(float DeltaTime, FBodyInstance* BodyInstance)
	{
	}
	void MyFunction() {
		int i = 1;
	}
	FString TestDirMy = FString(TEXT("C:/TestDir"));

	FString testFile = FString(TEXT("C:/TestDir/TestFileSVG.txt")); 

	bool VerifyOrCreateDirectory(const FString& TestDir);
	bool CreateFile(const FString& TestDir);

protected:
	UMyActorComponent();
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
