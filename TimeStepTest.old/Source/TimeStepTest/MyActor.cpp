// Fill out your copyright notice in the Description page of Project Settings.

#include "TimeStepTest.h"
#include "MyActor.h"

#include "PhysXIncludes.h"
#include "PhysicsPublic.h"
#include "Runtime/Engine/Private/PhysicsEngine/PhysXSupport.h"
#include "MyActorComponent.h"


// Sets default values
AMyActor::AMyActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UStaticMesh> WuefelMesh(TEXT("StaticMesh'/Engine/BasicShapes/Cube'"));
	UStaticMesh* BlankerWuerfel = WuefelMesh.Object;
	Wuerfel = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("root"));
	Wuerfel->SetStaticMesh(BlankerWuerfel);
	RootComponent = Wuerfel;w

	Wuerfel->SetSimulatePhysics(true);
	Wuerfel->WakeRigidBody();
	
	
	

	
	

}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

