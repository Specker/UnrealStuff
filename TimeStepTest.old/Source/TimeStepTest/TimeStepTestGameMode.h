// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "TimeStepTestGameMode.generated.h"

UCLASS(minimalapi)
class ATimeStepTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATimeStepTestGameMode();
};



