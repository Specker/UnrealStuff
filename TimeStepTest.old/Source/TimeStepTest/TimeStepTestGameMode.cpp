// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TimeStepTest.h"
#include "TimeStepTestGameMode.h"
#include "TimeStepTestHUD.h"
#include "TimeStepTestCharacter.h"

ATimeStepTestGameMode::ATimeStepTestGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATimeStepTestHUD::StaticClass();
}
