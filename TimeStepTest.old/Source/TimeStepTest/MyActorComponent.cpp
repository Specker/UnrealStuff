// Fill out your copyright notice in the Description page of Project Settings.

#include "TimeStepTest.h"
#include "MyActorComponent.h"

#include "PhysXIncludes.h"
#include "PhysicsPublic.h"
#include "Runtime/Engine/Private/PhysicsEngine/PhysXSupport.h"


// Sets default values for this component's properties
UMyActorComponent::UMyActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	 
}


// Called when the game starts
void UMyActorComponent::BeginPlay()
{
	Super::BeginPlay();
	//bool a = VerifyOrCreateDirectory(TestDirMy);
	//bool b = CreateFile(testFile);

	

}


// Called every frame
void UMyActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	

	// ...
}

bool UMyActorComponent::VerifyOrCreateDirectory(const FString& TestDir)
{
	// Every function call, unless the function is inline, adds a small
	// overhead which we can avoid by creating a local variable like so.
	// But beware of making every function inline!
	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

	// Directory Exists?
	if (!PlatformFile.DirectoryExists(*TestDir))
	{
		PlatformFile.CreateDirectory(*TestDir);
		if (!PlatformFile.DirectoryExists(*TestDir))
		{
			return false;
			//~~~~~~~~~~~~~~
		}
	}
	return true;
}

bool UMyActorComponent::CreateFile(const FString& TestFile) {

	IPlatformFile& pfFile = FPlatformFileManager::Get().GetPlatformFile();
	IFileHandle* fHandle = pfFile.OpenWrite(*TestFile);


	if (fHandle) {
		fHandle->~IFileHandle();
	}
	int64 sizeIs = pfFile.FileSize(*TestFile);

	FString myFileLoaded = "default load";
	FFileHelper::LoadFileToString(myFileLoaded, *TestFile);




	//if not exists, create
	//if (!FPlatformFileManager::Get().GetPlatformFile().FileExists(*TestFile)) {

	FVector myVec = FVector(1, 2, 3);

	FString saver = FString("habbaaaa");

	FFileHelper::SaveStringToFile(saver, *TestFile);

	FString logTemo = FString::FromInt(sizeIs);

	UE_LOG(LogTemp, Warning, TEXT(" The textfile is %s"), *myFileLoaded);
	UE_LOG(LogTemp, Warning, TEXT("%i"), *logTemo);

	//}1889671048
	//}1889668776

	


	return true;
}

